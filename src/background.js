"use strict";

import { app, protocol, BrowserWindow, ipcMain } from "electron";
const path = require("path");
import {
  createProtocol,
  installVueDevtools
} from "vue-cli-plugin-electron-builder/lib";
const isDevelopment = process.env.NODE_ENV !== "production";

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win;
let sqlite3 = require("sqlite3").verbose();
const dbPath = path.resolve(__dirname, "personal.sqlite");

// Standard scheme must be registered before the app is ready
protocol.registerStandardSchemes(["app"], { secure: true });
function createWindow() {
  // Create the browser window.
  win = new BrowserWindow({ width: 800, height: 600 });

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    // Load the url of the dev server if in development mode
    win.loadURL(process.env.WEBPACK_DEV_SERVER_URL);
    /* if (!process.env.IS_TEST) win.webContents.openDevTools() */
  } else {
    createProtocol("app");
    // Load the index.html when not in development
    win.loadURL("app://./index.html");
  }

  /* let db = new sqlite3.Database(dbPath);
  db.serialize(function () {
    db.run("CREATE TABLE persona (_id INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT, correo TEXT)");
  });
  db.close(); */

  ipcMain.on("load", (event, arg) => {
    console.log(arg);
    let db = new sqlite3.Database(dbPath);
    db.serialize(function() {
      getList(event, db);
    });
    db.close();
  });

  ipcMain.on("save", (event, person) => {
    let db = new sqlite3.Database(dbPath);
    db.serialize(function() {
      db.run("INSERT INTO persona(nombre, correo) VALUES(?, ?)", [
        person.nombre,
        person.correo
      ]);
      getList(event, db);
    });
    db.close();
  });

  ipcMain.on("upd", (event, person) => {
    let db = new sqlite3.Database(dbPath);
    db.serialize(function() {
      db.run("UPDATE persona SET nombre=?, correo=? WHERE _id=?", [
        person.nombre,
        person.correo,
        person.id
      ]);
      getList(event, db);
    });
    db.close();
  });

  ipcMain.on("del", (event, person_id) => {
    let db = new sqlite3.Database(dbPath);
    db.serialize(function() {
      db.run("DELETE FROM persona WHERE _id=?", [person_id]);
      /* db.all("SELECT _id as id, nombre, correo FROM persona", function(err, rows) {
        win.webContents.send('list', rows);
      }); */
      getList(event, db);
    });
    db.close();
  });

  const getList = function(mevent, mdb) {
    mdb.all("SELECT _id as id, nombre, correo FROM persona", function(
      err,
      rows
    ) {
      mevent.sender.send("list", rows);
    });
  };

  win.on("closed", () => {
    win = null;
  });
}

// Quit when all windows are closed.
app.on("window-all-closed", () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow();
  }
});

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", async () => {
  if (isDevelopment && !process.env.IS_TEST) {
    // Install Vue Devtools
    try {
      await installVueDevtools();
    } catch (e) {
      console.error("Vue Devtools failed to install:", e.toString());
    }
  }
  createWindow();
});

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
  if (process.platform === "win32") {
    process.on("message", data => {
      if (data === "graceful-exit") {
        app.quit();
      }
    });
  } else {
    process.on("SIGTERM", () => {
      app.quit();
    });
  }
}
