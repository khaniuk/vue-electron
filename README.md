# Vue + Electron + SQLite

[Vuejs](https://vuejs.org/) web application brought to the desktop using the framwork [Electronjs](https://www.electronjs.org/), with data base [SQLite](https://www.sqlite.org/).

## Project setup

Install dependencies

```
npm install
```

Setting or verify file **src/main.js** for electron **_true_** and for web **_false_**.

```
const isElectron = false;   //Web app

const isElectron = true;    //Electron app
```

## Before compiling electron

```
npm run rebuild             //SQLite rebuild
```

Location in **dist_electron/personal.sqlite**

## Compiles and hot-reloads for development

```
npm run serve               //Web Application

npm run electron:serve      //Electron Application
```

## Copy DB SQLite

The directory **_dist_electron_** generate automatically.

```
cp db/personal.sqlite dist_electron/personal.sqlite
```

## Compiles and minifies for production

```
npm run build               //Web Application

npm run electron:build      //Electron Application
```

### Run your tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```
